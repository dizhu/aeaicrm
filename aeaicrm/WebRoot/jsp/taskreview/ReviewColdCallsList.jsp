<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>陌生拜访</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript">
var operaRequestBox;
function openContentRequestBox(operaType,title,handlerId,subPKField){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (!operaRequestBox){
		operaRequestBox = new PopupBox('operaRequestBox',title,{size:'big',height:'600px',width:'1050',top:'3px',scroll:'yes'});
	}
	var url = 'index?'+handlerId+'&operaType='+operaType+'&'+subPKField+'='+$("#"+subPKField).val();
	operaRequestBox.sendRequest(url);
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table class="toolBar" border="0" cellpadding="0" cellspacing="1">
<tr>
<%if ("SubmitPlan".equals(pageBean.inputValue("TASK_REVIEW_STATE"))){%>
   	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'confirmPlan'})"><input value="&nbsp;" type="button" class="confirmImgBtn" title="确认计划" />确认计划</td>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'repulsePlan'})"><input value="&nbsp;" type="button" class="removeImgBtn" title="打回计划" />打回计划</td>
<%} %>     
</tr>
</table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="陌生拜访.csv"
retrieveRowsCallback="process" xlsFileName="陌生拜访.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |extend|status"
width="100%" rowsDisplayed="15"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="openContentRequestBox('detail','潜在客户','ReviewColdCallsEdit','ORG_ID')" oncontextmenu="selectRow(this,{ORG_ID:'${row.ORG_ID}'});refreshConextmenu()" onclick="selectRow(this,{ORG_ID:'${row.ORG_ID}'})">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="120" property="ORG_NAME" title="客户名称" />
	<ec:column width="80" property="ORG_CLASSIFICATION" title="分类" mappingItem="ORG_CLASSIFICATION"/>
	<ec:column width="50" property="ORG_STATE" title="状态" mappingItem="ORG_STATE"/>
	<ec:column width="80" property="ORG_CREATER_NAME" title="创建人" />
	<ec:column width="100" property="ORG_CREATE_TIME" title="创建时间" />
    <ec:column width="100" property="ORG_UPDATE_TIME" title="更新时间" />
	<ec:column width="120" property="ORG_LABELS_NAME" title="标签" />
	<ec:column width="120" property="TASK_FOLLOW_STATE" title="跟进状态" mappingItem="TASK_FOLLOW_STATE" />
</ec:row>
</ec:table>
<input type="hidden" name="TASK_ID" id="TASK_ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<input type="hidden" name="ORG_ID" id="ORG_ID" value="" />
<input type="hidden" name="TASK_REVIEW_ID" id="TASK_REVIEW_ID" value="<%=pageBean.inputValue("TASK_REVIEW_ID")%>" />
<input type="hidden" name="TC_ID" id="TC_ID" value="<%=pageBean.inputValue("TC_ID")%>" />
<input type="hidden" name="SALE_ID" id="SALE_ID" value="<%=pageBean.inputValue("SALE_ID")%>" />
<input type="hidden" name="ids" id="ids" value="<%=pageBean.inputValue("ids")%>" />
<script language="JavaScript">
setRsIdTag('ORG_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
