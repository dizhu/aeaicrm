package com.agileai.crm.cxmodule;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.MasterSubService;

public interface FollowUpManage
        extends MasterSubService {
	DataRow getMyCustRecord(DataParam param);
	void createCustVisitRecord(DataParam param);
	void updateTaskStateRecord(DataParam param);
	void updateTaskClassRecord(DataParam param);
	DataRow getCustInfoRecord(DataParam param);
	void updateCustStateRecord(DataParam param);
	void createCustRecord(DataParam param);
	DataRow getTaskReviewRecord(DataParam param);
	DataRow getCustVisitInfoRecord(DataParam param);
	DataRow getCustPhoneInfoRecord(DataParam param);
}
