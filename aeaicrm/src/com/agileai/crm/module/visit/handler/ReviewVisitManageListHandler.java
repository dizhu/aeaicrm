package com.agileai.crm.module.visit.handler;

import java.util.Date;
import java.util.List;

import com.agileai.crm.cxmodule.VisitManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.DispatchRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;

public class ReviewVisitManageListHandler extends StandardListHandler {
	public ReviewVisitManageListHandler() {
		super();
		this.editHandlerClazz = ReviewVisitManageEditHandler.class;
		this.serviceId = buildServiceId(VisitManage.class);
	}
	
	public ViewRenderer prepareDisplay(DataParam param){
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		List<DataRow> rsList = getService().findRecords(param);
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}

	protected void processPageAttributes(DataParam param) {
		initMappingItem("VISIT_TYPE", FormSelectFactory.create("VISIT_TYPE")
				.getContent());
		initMappingItem("VISIT_STATE", FormSelectFactory.create("CUST_STATE")
				.getContent());
		initMappingItem("VISIT_EFFECT", FormSelectFactory.create("VISIT_EFFECT").getContent());
		
		setAttribute("VISIT_TYPE",
		           FormSelectFactory.create("VISIT_TYPE")
		                            .addSelectedValue(param.get("VISIT_TYPE")));
		setAttribute("VISIT_EFFECT",
		           FormSelectFactory.create("VISIT_EFFECT")
		                            .addSelectedValue(param.get("VISIT_EFFECT")));
	}

	protected void initParameters(DataParam param) {
		initParamItem(
				param,
				"sdate",
				DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,
						DateUtil.getDateAdd(DateUtil.getBeginOfMonth(new Date()),DateUtil.YEAR,-5)));
		initParamItem(
				param,
				"edate",
				DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,
						DateUtil.getDateAdd(new Date(), DateUtil.DAY, 1)));
		initParamItem(param, "visitCustName", "");
		initParamItem(param, "visitUserName", "");
	}

	@PageAction
	public ViewRenderer doConfirmRequestAction(DataParam param) {
		storeParam(param);
		return new DispatchRenderer(getHandlerURL(this.editHandlerClazz) + "&"
				+ OperaType.KEY + "=confirm&comeFrome=confirm");
	}

	@PageAction
	public ViewRenderer doCounterConfirmRequestAction(DataParam param) {
		storeParam(param);
		return new DispatchRenderer(getHandlerURL(this.editHandlerClazz) + "&"
				+ OperaType.KEY + "=counterConfirm&comeFrome=counterConfirm");
	}
	
    @PageAction
    public ViewRenderer doCreateClueAction(DataParam param) {
		storeParam(param);
		String url = "VisitCreateClueEdit";
		return new DispatchRenderer(getHandlerURL(url) + "&"
				+ OperaType.KEY + "=doCreateClueAction&comeFrome=doCreateClueAction");
	}

	protected VisitManage getService() {
		return (VisitManage) this.lookupService(this.getServiceId());
	}
}
