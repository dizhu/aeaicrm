package com.agileai.crm.module.visit.handler;

import java.util.List;

import com.agileai.crm.common.PrivilegeHelper;
import com.agileai.crm.module.visit.service.VisitListSelect;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.PickFillModelHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class VisitListSelectListHandler
        extends PickFillModelHandler {
    public VisitListSelectListHandler() {
        super();
        this.serviceId = buildServiceId(VisitListSelect.class);
    }
    public ViewRenderer prepareDisplay(DataParam param){
    	User user = (User) this.getUser();
		PrivilegeHelper privilegeHelper = new PrivilegeHelper(user);
		if (!privilegeHelper.isSalesDirector()) {
			param.put("currentUserCode", user.getUserId());
			setAttribute("hasRight", false);
		} else {
			param.put("currentUserCode", "");
			setAttribute("hasRight", true);
		}
    	
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		List<DataRow> rsList = getService().queryPickFillRecords(param);
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}

    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "custName", "");
    }

    protected VisitListSelect getService() {
        return (VisitListSelect) this.lookupService(this.getServiceId());
    }
}
