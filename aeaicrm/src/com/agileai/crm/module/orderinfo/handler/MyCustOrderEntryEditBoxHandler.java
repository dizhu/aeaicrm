package com.agileai.crm.module.orderinfo.handler;

import java.math.BigDecimal;

import com.agileai.crm.cxmodule.OrderInfoManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.MasterSubEditPboxHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.StringUtil;

public class MyCustOrderEntryEditBoxHandler
        extends MasterSubEditPboxHandler {
    public MyCustOrderEntryEditBoxHandler() {
        super();
        this.serviceId = buildServiceId(OrderInfoManage.class);
        this.subTableId = "OrderEntry";
    }

    public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		if (isReqRecordOperaType(operaType)){
			DataRow record = getService().getSubRecord(subTableId, param);
			this.setAttributes(record);			
		}if("insert".equals(operaType)){
			setAttribute("doEdit8Save", true);
		}if("update".equals(operaType)){
			setAttribute("doEdit8Save", true);
		}
		if("detail".equals(operaType)){
			DataRow record = getService().getSubRecord(subTableId, param);
			if(record.get("ORDER_STATE").equals("0")){
				setAttribute("doEdit8Save", true);
			}else{
				setAttribute("doEdit8Save", false);
			}
			this.setAttributes(record);	
		}
		this.setOperaType(operaType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    protected void processPageAttributes(DataParam param) {
        if (!StringUtil.isNullOrEmpty(param.get("ORDER_ID"))) {
            this.setAttribute("ORDER_ID", param.get("ORDER_ID"));
        }
        
		if (this.getAttribute("ENTRY_UNIT_PRICE") == null) {
			this.setAttribute("ENTRY_UNIT_PRICE", new BigDecimal("0.00"));
		}
		if (this.getAttribute("ENTRY_DISCOUNT") == null) {
			this.setAttribute("ENTRY_DISCOUNT",new BigDecimal("1.00"));
		}
		if (this.getAttribute("ENTRY_NUMBER") == null) {
			this.setAttribute("ENTRY_NUMBER",new BigDecimal("1.00"));
		}
		if (this.getAttribute("ENTRY_REAL_PRICE") == null) {
			this.setAttribute("ENTRY_REAL_PRICE",new BigDecimal("0.00"));
		}
	}
    
    public ViewRenderer doSaveAction(DataParam param){
		String responseText = FAIL;
		String operateType = param.get(OperaType.KEY);
		if (OperaType.CREATE.equals(operateType)){
			getService().createSubRecord(subTableId, param);
			responseText = SUCCESS;
		}
		else if (OperaType.UPDATE.equals(operateType)){
			getService().updateSubRecord(subTableId, param);
			responseText = SUCCESS;
		}else if (OperaType.DETAIL.equals(operateType)){
			getService().updateSubRecord(subTableId, param);
			responseText = SUCCESS;
		}
		String masterRecordId = param.get("ORDER_ID");
		String entryId = param.get("ENTRY_ID");
		this.getService().computeTotalMoney(masterRecordId,entryId);
		return new AjaxRenderer(responseText);
	}

    protected OrderInfoManage getService() {
        return (OrderInfoManage) this.lookupService(this.getServiceId());
    }
}
