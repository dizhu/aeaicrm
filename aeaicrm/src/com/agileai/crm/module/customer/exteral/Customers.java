package com.agileai.crm.module.customer.exteral;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/rest")
public interface Customers {
	
	@POST  
    @Path("/list")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
	public String findMyCustomersList(String searchWord);
    
    @GET  
    @Path("/list-for-name/{searchWord}")
    @Produces(MediaType.TEXT_PLAIN)
	public String findMyCustForNameList(@PathParam("searchWord") String searchWord);
    
    @GET
    @Path("/get-details/{id}")  
    @Produces(MediaType.TEXT_PLAIN)
	public String getCustomersDetails(@PathParam("id") String id);   
    
    @POST
    @Path("/add-cust-info")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
	public String createCustomers(String info);
    
    @POST
    @Path("/update-cust-info")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
	public String editCustomersDetails(String info);
    
    @GET  
    @Path("/delete-info/{id}")
    @Produces(MediaType.TEXT_PLAIN)
	public String deleteCustomersInfo(@PathParam("id") String id);
    
    @GET
    @Path("/get-info/{id}")  
    @Produces(MediaType.TEXT_PLAIN)
	public String getCustomersInfo(@PathParam("id") String id);
    
    @GET
    @Path("/update-cust-state/{id}/{state}")  
    @Produces(MediaType.TEXT_PLAIN)
	public String updateCustStateInfo(@PathParam("id") String id, @PathParam("state") String state);
    
    @GET  
    @Path("/find-cust-visit/{custId}")
    @Produces(MediaType.TEXT_PLAIN)
	public String findCustomersVisitInfo(@PathParam("custId") String custId);
    
    @POST
    @Path("/add-cust-visit")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
	public String createCustomersVisitInfo(String info);
    
    @GET  
    @Path("/delete-visit-info/{id}")
    @Produces(MediaType.TEXT_PLAIN)
	public String deleteCustomersVisitInfo(@PathParam("id") String id);
    
    @GET
    @Path("/get-visit-info/{id}")
    @Produces(MediaType.TEXT_PLAIN)
	public String getCustomersVisitInfo(@PathParam("id") String id);
    
    @POST
    @Path("/update-visit-info")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
	public String editCustomersVisitInfo(String info);
    
    @GET
    @Path("/submit-visit-state/{id}")
    @Produces(MediaType.TEXT_PLAIN)
	public String submitVisitStateInfo(@PathParam("id") String id);
    
    @GET  
    @Path("/find-cust-opp/{custId}")
    @Produces(MediaType.TEXT_PLAIN)
	public String findCustomersOppInfo(@PathParam("custId") String custId);
    
    @GET
    @Path("/get-opp-info/{id}")
    @Produces(MediaType.TEXT_PLAIN)
	public String getCustomersOppInfo(@PathParam("id") String id);
    
    @POST
    @Path("/update-opp-info")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
	public String editCustomersOppInfo(String info);
    
    @POST
    @Path("/add-cust-opp")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
	public String createCustomersOppInfo(String info);
    
    @GET  
    @Path("/delete-opp-info/{id}")
    @Produces(MediaType.TEXT_PLAIN)
	public String deleteCustomersOppInfo(@PathParam("id") String id);
    
    @GET
    @Path("/update-opp-state/{id}/{state}")  
    @Produces(MediaType.TEXT_PLAIN)
	public String updateOppStateInfo(@PathParam("id") String id, @PathParam("state") String state);
    
    @GET  
    @Path("/find-cust-contacts/{custId}")
    @Produces(MediaType.TEXT_PLAIN)
	public String findCustomersContactsInfo(@PathParam("custId") String custId);
    
    @GET
    @Path("/get-contacts-info/{id}")
    @Produces(MediaType.TEXT_PLAIN)
	public String getCustomersContInfo(@PathParam("id") String id);
    
    @POST
    @Path("/add-cust-contacts")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
	public String createCustomersContactsInfo(String info);
    
    @POST
    @Path("/update-contacts-info")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
	public String editCustomersContactsInfo(String info);
    
    @GET  
    @Path("/delete-contacts-info/{id}")
    @Produces(MediaType.TEXT_PLAIN)
	public String deleteCustomersContactsInfo(@PathParam("id") String id);
    
    @GET  
    @Path("/find-cust-order/{custId}")
    @Produces(MediaType.TEXT_PLAIN)
	public String findCustomersOrderInfo(@PathParam("custId") String custId);
    
    @GET  
    @Path("/get-cust-order/{orderId}")
    @Produces(MediaType.TEXT_PLAIN)
	public String getCustomersOrderInfo(@PathParam("orderId") String orderId);
    
    @GET  
    @Path("/get-cust-order-entry/{id}")
    @Produces(MediaType.TEXT_PLAIN)
	public String getCustomersOrderEntryInfo(@PathParam("id") String id);
    
    @POST
    @Path("/add-cust-order")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
	public String createCustomersOrderInfo(String info);
    
    @POST
    @Path("/add-cust-order-entry")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
	public String createCustomersOrderEntryInfo(String info);
    
    @POST
    @Path("/update-cust-order")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
	public String updateCustomersOrderInfo(String info);
    
    @POST
    @Path("/update-cust-order-entry")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
	public String updateCustomersOrderEntryInfo(String info);
    
    @GET
    @Path("/update-order-state/{id}/{state}")  
    @Produces(MediaType.TEXT_PLAIN)
	public String updateOrderStateInfo(@PathParam("id") String id, @PathParam("state") String state);
    
    @GET  
    @Path("/delete-order-info/{id}")
    @Produces(MediaType.TEXT_PLAIN)
	public String deleteCustomersOrderInfo(@PathParam("id") String id);
    
    @GET  
    @Path("/delete-order-entry-info/{id}")
    @Produces(MediaType.TEXT_PLAIN)
	public String deleteCustomersOrderEntryInfo(@PathParam("id") String id);
    
    @GET  
    @Path("/find-select/")
    @Produces(MediaType.TEXT_PLAIN)
	public String findSelect();
    
    @GET  
    @Path("/find-cont-list/{custId}")
    @Produces(MediaType.TEXT_PLAIN)
	public String findContList(@PathParam("custId") String custId);
    
    @GET
    @Path("/home-card-list/")
    @Produces(MediaType.TEXT_PLAIN)
	public String findHomeCardRecords();
    
    @GET
    @Path("/find-opp-order-list/{saleId}")
    @Produces(MediaType.TEXT_PLAIN)
	public String findOppOrderRecords(@PathParam("saleId") String saleId);
    
    @GET
    @Path("/home-card-saleList/")
    @Produces(MediaType.TEXT_PLAIN)
	public String findHomeCardSaleListRecords();
}
