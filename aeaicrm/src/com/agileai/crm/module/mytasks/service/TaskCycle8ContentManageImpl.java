package com.agileai.crm.module.mytasks.service;


import java.util.List;

import com.agileai.common.KeyGenerator;
import com.agileai.crm.cxmodule.TaskCycle8ContentManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManageImpl;

public class TaskCycle8ContentManageImpl
        extends TreeAndContentManageImpl
        implements TaskCycle8ContentManage {
    public TaskCycle8ContentManageImpl() {
        super();
        this.columnIdField = "TC_ID";
        this.columnParentIdField = "TC_FID";
        this.columnSortField = "TC_BEGIN";
    }

	@Override
	public void generatPlanRecord(DataParam param) {
		String taskReviewId = KeyGenerator.instance().genKey();
		param.put("TASK_REVIEW_ID", taskReviewId);
		String statementId = this.sqlNameSpace+"."+"generatPlanRecord";
		this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public List<DataRow> findProPlanVisitRecords(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"findProPlanVisitRecords";
		List<DataRow> rsList = this.daoHelper.queryRecords(statementId, param);
		return rsList;
	}

	@Override
	public List<DataRow> findCustPlanVisitRecords(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"findCustPlanVisitRecords";
		List<DataRow> rsList = this.daoHelper.queryRecords(statementId, param);
		return rsList;
	}
	
	@Override
	public List<DataRow> queryPeriodicTreeRecords(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"queryPeriodicTreeRecords";
		List<DataRow> rsList = this.daoHelper.queryRecords(statementId, param);
		return rsList;
	}

	@Override
	public List<DataRow> queryUnfinishedTaskRecords(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"queryUnfinishedTaskRecords";
		List<DataRow> rsList = this.daoHelper.queryRecords(statementId, param);
		return rsList;
	}

	@Override
	public DataRow getSummaryRecord(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"getSummaryRecord";
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
	}
	
	@Override
	public DataRow getProNumRecord(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"getProNumRecord";
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
	}
	
	@Override
	public DataRow getCustNumRecord(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"getCustNumRecord";
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
	}

	@Override
	public void updateSummaryRecord(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"updateSummaryRecord";
		this.daoHelper.updateRecord(statementId, param);
	}
	
	@Override
	public void aggregateyRecord(String taskReviewId,String orderNum,String oppNum) {
		DataParam param = new DataParam("TASK_REVIEW_ID",taskReviewId,"ORDER_NUM",orderNum,"OPP_NUM",oppNum);
		String statementId = this.sqlNameSpace+"."+"getVisitTotalRecord";
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		long visitTotal =  (Long) dataRow.get("TASK_REVIEW_VISITS_TOTAL");
		param.put("TASK_REVIEW_VISITS_TOTAL", visitTotal);
		
		statementId = this.sqlNameSpace+"."+"getStrangeTotalRecord";
		dataRow = this.daoHelper.getRecord(statementId, param);
		long strangeTotal =  (Long) dataRow.get("TASK_REVIEW_STRANGE");
		param.put("TASK_REVIEW_STRANGE", strangeTotal);
		
		statementId = this.sqlNameSpace+"."+"getStrangeTaskActualRecord";
		dataRow = this.daoHelper.getRecord(statementId, param);
		long strangeTaskActual =  (Long) dataRow.get("TASK_REVIEW_STRANGE_ACTUAL");
		param.put("TASK_REVIEW_STRANGE_ACTUAL", strangeTaskActual);
		
		statementId = this.sqlNameSpace+"."+"getStrangeFollowTotalRecord";
		dataRow = this.daoHelper.getRecord(statementId, param);
		long strangeFollowTotal =  (Long) dataRow.get("TASK_REVIEW_STRANGE_FOLLOW");
		param.put("TASK_REVIEW_STRANGE_FOLLOW", strangeFollowTotal);
		
		statementId = this.sqlNameSpace+"."+"getStrangeVisitTotalRecord";
		dataRow = this.daoHelper.getRecord(statementId, param);
		long strangeVisitTotal =  (Long) dataRow.get("TASK_REVIEW_STRANGE_VISIT");
		param.put("TASK_REVIEW_STRANGE_VISIT", strangeVisitTotal);
		
		statementId = this.sqlNameSpace+"."+"getIntentionTaskActualRecord";
		dataRow = this.daoHelper.getRecord(statementId, param);
		long intentionTaskActual =  (Long) dataRow.get("TASK_REVIEW_INTENTION_ACTUAL");
		param.put("TASK_REVIEW_INTENTION_ACTUAL", intentionTaskActual);
		
		statementId = this.sqlNameSpace+"."+"getIntentionFollowTotalRecord";
		dataRow = this.daoHelper.getRecord(statementId, param);
		long intentionFollowTotal =  (Long) dataRow.get("TASK_REVIEW_INTENTION_FOLLOW");
		param.put("TASK_REVIEW_INTENTION_FOLLOW", intentionFollowTotal);
		
		statementId = this.sqlNameSpace+"."+"getIntentionVisitTotalRecord";
		dataRow = this.daoHelper.getRecord(statementId, param);
		long intentionVisitTotal =  (Long) dataRow.get("TASK_REVIEW_INTENTION_VISIT");
		param.put("TASK_REVIEW_INTENTION_VISIT", intentionVisitTotal);
		
		statementId = this.sqlNameSpace+"."+"aggregateyRecord";
		this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public void updatePlanRecord(String taskReviewId,String taskReviewState) {
		DataParam param = new DataParam("TASK_REVIEW_STATE",taskReviewState,"TASK_REVIEW_ID",taskReviewId);
		String statementId = this.sqlNameSpace+"."+"updatePlanRecord";
		this.daoHelper.updateRecord(statementId, param);
	}
	
	public void planTaskRecord(String taskReviewId) {
		DataParam param = new DataParam("TASK_REVIEW_ID",taskReviewId);
		String statementId = this.sqlNameSpace+"."+"getStrangeTaskTotalRecord";
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		long strangeTaskTotal =  (Long) dataRow.get("TASK_REVIEW_STRANGE_TASK");
		param.put("TASK_REVIEW_STRANGE_TASK", strangeTaskTotal);
		
		statementId = this.sqlNameSpace+"."+"getIntentionTaskTotalRecord";
		dataRow = this.daoHelper.getRecord(statementId, param);
		long intentionTaskTotal =  (Long) dataRow.get("TASK_REVIEW_INTENTION_TASK");
		param.put("TASK_REVIEW_INTENTION_TASK", intentionTaskTotal);
		
		statementId = this.sqlNameSpace+"."+"planTaskRecord";
		this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public List<DataRow> queryOppRecords(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"queryOppRecords";
		List<DataRow> rsList = this.daoHelper.queryRecords(statementId, param);
		return rsList;
	}

	@Override
	public List<DataRow> queryOrderRecords(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"queryOrderRecords";
		List<DataRow> rsList = this.daoHelper.queryRecords(statementId, param);
		return rsList;
	}

	@Override
	public void deleteTaskReviewRecord(String columnId) {
		DataParam param = new DataParam("TASK_REVIEW_ID",columnId);
		String statementId = this.sqlNameSpace+"."+"deleteTaskReviewRecord";
		this.daoHelper.deleteRecords(statementId, param);
		deleteTaskReviewRelationRecord(columnId);
	}
	public void deleteTaskReviewRelationRecord(String columnId) {
		DataParam param = new DataParam("TASK_REVIEW_ID",columnId);
		String statementId = this.sqlNameSpace+"."+"deleteTaskReviewRelationRecord";
		this.daoHelper.deleteRecords(statementId, param);
	}

	@Override
	public void updateTaskReviewDescRecord(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"updateTaskReviewDescRecord";
		this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public List<DataRow> findHomeCardRecords(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"findHomeCardRecords";
		List<DataRow> rsList = this.daoHelper.queryRecords(statementId, param);
		return rsList;
	}

}
